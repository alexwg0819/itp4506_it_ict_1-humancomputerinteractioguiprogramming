-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 19, 2019 at 04:00 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ITP4506`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menuID` int(255) NOT NULL,
  `restaurantID` int(100) NOT NULL,
  `status` int(1) NOT NULL,
  `menuName` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menuID`, `restaurantID`, `status`, `menuName`) VALUES
(1, 1, 1, 'Breakfast'),
(2, 1, 1, 'Lunch'),
(4, 1, 1, 'Dinner');

-- --------------------------------------------------------

--
-- Table structure for table `menuDatils`
--

CREATE TABLE `menuDatils` (
  `foodID` int(11) NOT NULL,
  `menuID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Price` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menuDatils`
--

INSERT INTO `menuDatils` (`foodID`, `menuID`, `Name`, `Price`, `status`) VALUES
(1, 1, 'egg', 8, 1),
(2, 1, 'Pork', 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `restaurant`
--

CREATE TABLE `restaurant` (
  `restaurantID` int(100) NOT NULL,
  `HKI` tinyint(1) NOT NULL,
  `KL` tinyint(1) NOT NULL,
  `NT` tinyint(1) NOT NULL,
  `OI` tinyint(1) NOT NULL,
  `JP` tinyint(1) NOT NULL,
  `HK` tinyint(1) NOT NULL,
  `KR` tinyint(1) NOT NULL,
  `TI` tinyint(1) NOT NULL,
  `telephone` int(8) NOT NULL,
  `menuID` int(100) NOT NULL,
  `RANK` int(1) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Address` varchar(500) NOT NULL,
  `ownerID` int(10) NOT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `telephone2` int(11) DEFAULT NULL,
  `happy` int(6) NOT NULL,
  `angry` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant`
--

INSERT INTO `restaurant` (`restaurantID`, `HKI`, `KL`, `NT`, `OI`, `JP`, `HK`, `KR`, `TI`, `telephone`, `menuID`, `RANK`, `Name`, `Address`, `ownerID`, `Address2`, `telephone2`, `happy`, `angry`) VALUES
(1, 0, 0, 1, 0, 1, 1, 0, 0, 27999810, 1, 5, 'Chef\'s Stage Kitchen', 'Shop 1056D&1058, 1/F, Discovery Park, 398 Castle Peak Road, Was', 1, 'Shop 12A, 12/F, on9 Park, 398 Hello Peak Road, Was', 21432943, 283, 64),
(2, 1, 0, 0, 0, 0, 1, 1, 0, 28909130, 2, 4, 'Burgeroom', 'Shop D, G/F, Food Street, 50-56 Paterson Street, Fashion Walk, Causeway Bay', 2, '', NULL, 728, 201),
(3, 0, 1, 1, 0, 0, 1, 1, 0, 24812839, 3, 3, 'Lam Pung Chung', 'Ty,qweqweqwelkajsldjaskdk', 3, NULL, NULL, 132, 20),
(4, 0, 0, 0, 1, 0, 0, 1, 1, 27463821, 4, 3, 'Colorful Japanese Lunch Set', 'Shop B222, B2/F, Times Square, 1 Matheson Street, Causeway Bay', 2, NULL, NULL, 238, 230),
(5, 1, 0, 0, 1, 0, 1, 0, 1, 24238174, 5, 2, 'Curry & Kabab Hut', 'G/F, Front Portion, 19 Sung Kit Street, Hung Hom', 3, 'G/F, 39 Fuk Wa Street, Sham Shui Po', 63748283, 238, 230);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menuID`),
  ADD KEY `fk_menu_restaurant` (`restaurantID`);

--
-- Indexes for table `menuDatils`
--
ALTER TABLE `menuDatils`
  ADD PRIMARY KEY (`foodID`),
  ADD KEY `fk_datils_menu` (`menuID`);

--
-- Indexes for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`restaurantID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menuID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menuDatils`
--
ALTER TABLE `menuDatils`
  MODIFY `foodID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `restaurantID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `fk_menu_restaurant` FOREIGN KEY (`restaurantID`) REFERENCES `restaurant` (`restaurantID`);

--
-- Constraints for table `menuDatils`
--
ALTER TABLE `menuDatils`
  ADD CONSTRAINT `fk_datils_menu` FOREIGN KEY (`menuID`) REFERENCES `menu` (`menuID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="shortcut icon" type="image/x-icon" href="Logo.png" />
</head>

<body style=" background-color: gainsboro;">
<?php
session_start();
    if(isset($_SESSION["message"])){
        $message = $_SESSION["message"];
        echo "<script type='text/javascript'>alert('$message');</script>";
        unset($_SESSION["message"]);
    }

?>
    <nav class="navbar navbar-expand-lg  navbar-dark bg-dark" style="background: black">
        <a class="navbar-brand" href="#">SLMC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item ">
                    <a class="nav-link" href="main.php">Home </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="profile.php">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="viewOrder.php">MyOrder</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="shop.php">Shop</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link " href="login-2.php">Login<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="staffLogin.php">Staff Index</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container ">
        <div class="row">
            <div class="col-sm">
            </div>
            <div class="col-sm">
                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <h5 class="card-title d-flex justify-content-center">Login System</h5>
                        <form style="margin-top: 1em" method="POST" action="login_control.php">
                            <div class="form-group">
                                <label for="exampleInputEmail1">User ID</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your account">
                                <!--                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" name="exampleInputPassword1" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                            </div>
                            <div class="form-group d-flex justify-content-between">

                                <a href="">Register</a>

                                <a href="">Forgot Password</a>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm">
            </div>
        </div>
    </div>
</body>

</html>

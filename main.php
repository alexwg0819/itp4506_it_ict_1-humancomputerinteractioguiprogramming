<!DOCTYPE html>
<html lang="en">

<head>
<link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<?php session_start();?>


    <script type="text/javascript">

     var sort_fin_address = [];
     var sort_fin_food = [];
        $(function(){
            $( ".slider").slider({
                range: true,
                step:50,
                min:50,
                max:800,
                values: [ 50, 800, ],
                slide:function(event,ul){
                    $("#amount").val("( change )");
                }
            });
        })
        $(document).ready(function() {
            $("#amount").val("( ANY )");
            $("#favbox").hide();
            $("#favshow").click(function(){
                if ($(this).attr("passed")) {

                 $("#favbox").hide();
                 $(this).removeAttr("passed");
                 $(this).removeClass("favshow");
                  $("#hearticon").removeClass("abc2").addClass("abc");
                }else{

                 $(this).attr("passed", "passed");
                 $("#favbox").show('slow');
                 $("#hearticon").removeClass("abc").addClass("abc2");
                 $(this).addClass("favshow");
                }


            })
            $("#close1").click(function(){
                $("#rowfav1*").hide();
            })
            $("#close2").click(function(){
                $("#rowfav2*").hide();
            })
                     $("#close3").click(function(){
                $("#rowfav3*").hide();
            })

            var a = 0;
            $("#cr-tag").hide();
            $("#cr-bd").hide();

            $("#Addresstag*").appendTo("#start");
            $("#Cuisinels*").appendTo("#start2");
            // Address Sorting
            $("#search").on("keyup",function(){
                // for(i=1;i<6;i++){
                //     currentCard = "#cardCol" + i;
                //     $(currentCard).hide();
                // }
                // $("#cardCol2").show();
                var value=$(this).val().toLowerCase();
                $("article").filter(function(){
                    $(this).toggle($(this).text().toLowerCase().indexOf(value)>-1)
                });
            });

            $("#Addresstag*").click(function() {
                if ($(this).attr("passed")) {
                    $(this).appendTo("#start");
                    var deltaget = $(this).attr("passed");
                    $(deltaget).remove();
                    $(this).removeAttr("passed");
                    $(this).removeClass("remove_margie");
                    $(this).removeClass("text-center");
                    $(this).addClass("form-group");
                    $(this).addClass("form-check");
                    a -= 1;
                } else {
                    a += 1;
                    var htmlString = '<div class="btn btn-primary d-flex justify-content-between" style="padding:4px;width:90px;height:30px; display:flex;flex-direction:row;align-items:center"' + "id=tag" + a + ">" + '<span class="glyphicon glyphicon-remove-circle "style="margin-left:2px"></span>' + "</div>";
                    var taget = "#tag" + a;
                    var currentRow = checkEmpty();

                    $(currentRow).prepend(htmlString);
                    $(taget).prepend(this);

                    $(this).removeClass("form-group");
                    $(this).removeClass("form-check");

                    $(this).addClass("remove_margie text-center");
                    $(this).attr("passed", taget);
                    //checked = checked
                }
                if (a > 0) {
                    $("#cr-tag").show();
                    $("#cr-bd").show();
                } else if (a == 0) {
                    $("#cr-tag").hide();
                    $("#cr-bd").hide();
                }

                sort_fin_address = sortIngAddress();
                sort_fin_food = sortIngFood();
                // checksort();
                console.log(sort_fin_address);
                console.log(sort_fin_food);

                var currentCard;
            for (i=1;i<9;i++){
                 currentCard = "#cardCol" + i;
                $(currentCard).hide();
                $(currentCard).removeAttr("display");
                for(j=0;j<sort_fin_address.length;j++){
                    currentAddress = sort_fin_address[j];
                    console.log($(currentCard).attr(currentAddress));
                    if($(currentCard).attr(currentAddress)){
                        $(currentCard).show();
                        $(currentCard).attr("display","true");
                    }
                }

                for(j=0;j<sort_fin_food.length;j++){
                    currentfood = sort_fin_food[j];
                    if($(currentCard).attr(currentfood)){
                        $(currentCard).show();
                        $(currentCard).attr("display","true");
                    }
                }

            }
            if(sort_fin_address.length == 0 && sort_fin_food.length == 0){
                for(i=1;i<9;i++){
                    currentCard = "#cardCol" + i;
                    $(currentCard).show();
                }
            }

            });
            // Address End
            $("#Cuisinels*").click(function() {

                if ($(this).attr("passed")) {
                    $(this).appendTo("#start2");
                    var deltaget = $(this).attr("passed");
                    $(deltaget).remove();
                    $(this).removeAttr("passed");
                    $(this).removeClass("remove_margie");
                    $(this).removeClass("text-center");
                    $(this).addClass("form-group");
                    $(this).addClass("form-check");
                    a -= 1;
                } else {
                    a += 1;
                    var htmlString = '<div class="btn btn-warning d-flex justify-content-between" style="padding:4px;width:90px;height:30px; display:flex;flex-direction:row;align-items:center"' + "id=tag" + a + ">" + '<span class="glyphicon glyphicon-remove-circle "style="margin-left:2px"></span>' + "</div>";
                    var taget = "#tag" + a;
                    var currentRow = checkEmpty();

                    $(currentRow).prepend(htmlString);
                    $(taget).prepend(this);

                    $(this).removeClass("form-group");
                    $(this).removeClass("form-check");

                    $(this).addClass("remove_margie text-center");
                    $(this).attr("passed", taget);
                    //checked = checked
                }
                if (a > 0) {
                    $("#cr-tag").show();
                    $("#cr-bd").show();
                } else if (a == 0) {
                    $("#cr-tag").hide();
                    $("#cr-bd").hide();
                }
                sort_fin_address = sortIngAddress();
                sort_fin_food = sortIngFood();
                // checksort();
                console.log(sort_fin_address);
                console.log(sort_fin_food);

                var currentCard;
                for (i=1;i<9;i++){
                 currentCard = "#cardCol" + i;
                $(currentCard).hide();
                $(currentCard).removeAttr("display");

                for(j=0;j<sort_fin_address.length;j++){
                    currentAddress = sort_fin_address[j];
                    console.log($(currentCard).attr(currentAddress));
                    if($(currentCard).attr(currentAddress)){
                        $(currentCard).show();
                        $(currentCard).attr("display","true");
                    }
                }

                for(j=0;j<sort_fin_food.length;j++){
                    currentfood = sort_fin_food[j];
                    if($(currentCard).attr(currentfood)){
                        $(currentCard).show();
                        $(currentCard).attr("display","true");
                    }
                }

            }
            if(sort_fin_address.length == 0 && sort_fin_food.length == 0){
                for(i=1;i<9;i++){
                    currentCard = "#cardCol" + i;
                    $(currentCard).show();
                }
            }

            });

        });

        function checkEmpty() {
            for (i = 1; i < 9; i++) {
                var currentCK = "#r" + i;
                if ($(currentCK).children().length == 0) {
                    return currentCK;
                }
            }
        }

        function sortIngAddress() {
            var sort_adder = [];

            for (i = 1; i < 9; i++) {
                var currentCK = "#r" + i;
                var strAdree
                if ($(currentCK).children().length == 1) {
                    if ($(currentCK).children().children().attr("location")) {
                        strAdree = $(currentCK).children().children().attr("location");
                        sort_adder.push(strAdree);
                    }
                }
            }
            return sort_adder;
        }

        function sortIngFood() {
            var sort_food = [];
            for (i = 1; i < 9; i++) {
                var currentCK = "#r" + i;
                var strAdree
                if ($(currentCK).children().length == 1) {
                    if ($(currentCK).children().children().attr("food")) {
                        strAdree = $(currentCK).children().children().attr("food");
                        sort_food.push(strAdree);
                    }
                }
            }
            return sort_food;
        }
  ;
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body style=" background-color: white" style="overflow-x:hidden">
    <nav class="navbar navbar-expand-lg  navbar-dark " style="background: #343a40;margin-bottom:0;border-radius:0px;position:fixed;width:100%;z-index:1000;padding-bottom:0;border-bottom:0" >
        <a class="navbar-brand" href="#">YummyEveryWhere</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <?php
            $hostname = "127.0.0.1"; $username = "root";
            $pwd = ""; $db = "ITP4506";
            $conn = mysqli_connect($hostname, $username, $pwd, $db)
                or die(mysqli_connect_error());
                if(isset($_GET['page'])){
                  if($_GET['page']==1){
                    $sql = "SELECT * FROM `restaurant` WHERE `restaurantID` <= 6";
                  }else{$sql = "SELECT * FROM `restaurant` WHERE `restaurantID` > 6";}
                }else{$sql = "SELECT * FROM `restaurant` WHERE `restaurantID` <= 6";}

            $rs = mysqli_query($conn, $sql)
            or die(mysqli_error($conn));
        ?>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item ">
                    <a class="nav-link" href="main.php">Home </a>
                </li>
<?php if(isset($_SESSION['userID'])){?>
                <li class="nav-item ">
                    <a class="nav-link" href="Operator.html">My Restaurant</a>
                </li>
    <?php }?>
            </ul>
        </div>
        <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0 d-flex justify-content-end"style="width:100%">
                <?php
                    if(isset($_SESSION["message"])){
        $message = $_SESSION["message"];
        echo "<script type='text/javascript'>alert('$message');</script>";
        unset($_SESSION["message"]);
    }
                if(!isset($_SESSION["userID"])){?>
                <li class="nav-item ">
                    <a class="nav-link" href="login.php">Login </a>
                </li>
                <?php }else{?>
                <li class="nav-item d-flex align-items-center "style="margin-right:82px;width:50px" id="favshow">
                    <a class="dropdown-toggle nav-link" style="color:white"><i class="fa fa-heart abc " aria-hidden="true" style="font-size:20px;" id="hearticon"></i></a>
                </li>
                <li class="nav-item " style="margin-right:10px;">
                    <a class="nav-link" href="userDeail.html"><i class="fas fa-user" style="font-size:21px"></i>
 </a>
                </li>
                <li class="nav-item " style=" ">
                    <a class="nav-link" href="logout.php">Log Out </a>
                </li>
                <?php }?>
            </ul>
        </div>
    </nav>
    <div id="banner">
          <div class="d-flex justify-content-end"style="width:100%;margin-top:57px;position:fixed;z-index:200" ><div style="z-index:1;position:absolute;background-color:white;"id="favbox">
<div class="container" style="width:257.5px;padding-right:0;margin-top:8px;padding-left:10px" >

  <div class="row" id="rowfav1">
    <div class="col-3 " >
      <img src="img/1.jpg" style="height:50px;width:50px">
    </div>
    <div class="col-7 d-flex align-items-center" style="padding-right:0;padding-left:10px" >
      <a href="menu.php?id=1">Chef's Stage Kitchen </a>
    </div>
        <div class="col-1 d-flex align-items-center" style="padding-right:0;padding-left:0" >
     <i class="fa fa-times " aria-hidden="true" id="close1"></i>
    </div>
  </div>
  <hr style="margin-left:-10px;margin-top:10px;margin-bottom:0"id="rowfav1">
    <div class="row" style="margin-top:10px;margin-bottom:10px" id="rowfav2">

    <div class="col-3 " >
      <img src="img/2.jpg" style="height:50px;width:50px">
    </div>
    <div class="col-7 d-flex align-items-center" style="padding-right:0;padding-left:10px" >
      <a href="menu.php?id=2">Burgeroom</a>
    </div>
        <div class="col-1 d-flex align-items-center" style="padding-right:0;padding-left:0" >
     <i class="fa fa-times " aria-hidden="true" id="close2" ></i>
    </div>
  </div>
   <hr style="margin-left:-10px;margin-top:10px;margin-bottom:0" id="rowfav2" >
       <div class="row" style="margin-top:10px;margin-bottom:10px"id="rowfav3">

    <div class="col-3 " >
      <img src="img/5.jpg" style="height:50px;width:50px">
    </div>
    <div class="col-7 d-flex align-items-center" style="padding-right:0;padding-left:10px" >
      <a href="menu.php?id=5">Curry & Kabab Hut</a>
    </div>
        <div class="col-1 d-flex align-items-center" style="padding-right:0;padding-left:0" >
     <i class="fa fa-times " aria-hidden="true" id="close3"></i>
    </div>
  </div>
  </div>
</div>
        </div></div>
            <video preload="yes" autoplay="" loop="" muted=""  style="width:100%;height:5%;margin-top:-140px;z-index:1;">
                <source src="video/video.mp4" type="video/mp4">
            </video>
    </div>
    <div style="display: flex ; flex-direction: row ;z-index:2;margin-top:-80px">
        <div style="display: flex ; flex-direction: column ;margin-left:12px">
                    <div class="card text-center" style="width: 16em">
                <div class="card-header">
                    Search
                </div>
                <input style="texts"width="100%" placeholder="Search : Restaurant Name" id="search"></button>
            </div>
            <div class="card text-center">
                <div class="card-header" id="cr-tag" stype="padding:4px">
                    <div style="margin-bottom:-10px">Current Tag</div> <a id="clear" href="main.php" class="d-flex justify-content-end"style="color:blue;font-size:10px;margin-bottom:-4px;margin-right:-5px">Clear All</a>
                </div>
                <div class="card-body" style=" padding: 4px 4px 0px 4px" id="cr-bd">
                    <div class="container" style="margin-left: 0em;width: 100%;height: 500;" id="resul">
                        <div class="row " style="margin-bottom:5px ">
                            <div class="col d-flex justify-content-center" id="r1" style="padding:0">

                            </div>
                            <div class="col d-flex justify-content-center" id="r2" style="padding:0">

                            </div>
                        </div>
                        <div class="row" style="margin-bottom:5px ">
                            <div class="col d-flex justify-content-center" id="r3" style="padding:0">

                            </div>
                            <div class="col d-flex justify-content-center" id="r4" style="padding:0">

                            </div>
                        </div>
                        <div class="row" style="margin-bottom:5px ">
                            <div class="col d-flex justify-content-center" id="r5" style="padding:0">

                            </div>
                            <div class="col d-flex justify-content-center" id="r6" style="padding:0">

                            </div>
                        </div>
                        <div class="row" style="margin-bottom:5px ">
                            <div class="col d-flex justify-content-center" id="r7" style="padding:0">

                            </div>
                            <div class="col d-flex justify-content-center" id="r8" style="padding:0">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card text-center" style="width: 16em">
                <div class="card-header" style="padding:6px">
                    Your Location
                </div>
                <div class="card-body d-flex justify-content-between" style="padding:0px;  ">
                    <input type="text" id="demo" style="width:90%"/>
                    <button onclick="getLocation()" ><i class="fa fa-map-marker" aria-hidden="true" style="font-size:20px; margin:3px"  ></i></button>
                </div>
            </div>
            <div class="card text-center" style="width: 16em">
                <div class="card-header" style="padding:6px">
                    District
                </div>
                <div class="card-body" style="padding-top:8px;padding-bottom:0px;  ">
                    <div style="display: flex ;flex-direction: column ;align-items: flex-start;margin-left: 2em;width: 100%;height: 500;" id="start">
                    </div>

                </div>
            </div>
            <div class="card text-center" style="width: 16em">
                <div class="card-header">
                    Cuisine
                </div>
                <div class="card-body" style="padding-top:8px;padding-bottom:0px;  ">
                    <div style="display: flex ;flex-direction: column ;align-items: flex-start;margin-left: 2em;width: 100%;height: 500;" id="start2">
                    </div>

                </div>
            </div>
            <div class="card text-center" style="width: 16em">
                <div class="card-header d-flex flex-column bd-highlight " style="margin-bottom:0">
                    Price 
                    <input type="text" id="amount" style="background-color:transparent;width:100%;border:0;text-align:center">
                    
                </div>
                
                <div class="card-body" style="padding-top:0px;padding-bottom:8px;  ">
                
                <div class="slider"></div>

                </div>
            </div>
        </div>

        <div class="container" style="margin-top: 0em">

<div class="grid" style="margin-bottom:2em">
                <?php
                $colcount =0;
                                    while ($rc = mysqli_fetch_assoc($rs)) {
                                         $tel = $rc['telephone'];
                                         $rank = $rc['RANK'];
                                         $name = $rc['Name'];
                                         $address = $rc['Address'];
                                         $colcount++;
                ?>


                <article id="cardCol<?php echo $colcount; ?>"                    <?php if($rc['HKI']==1){echo 'HKI="HKI"';}?>
                                    <?php if($rc['KL']==1){echo 'KL="KL"';}?>
                                    <?php if($rc['NT']==1){echo 'NT="NT"';}?>
                                    <?php if($rc['OI']==1){echo 'OI="OI"';}?>
                                    <?php if($rc['JP']==1){echo 'JP="JP"';}?>
                                    <?php if($rc['HK']==1){echo 'HK="HK"';}?>
                                    <?php if($rc['KR']==1){echo 'KR="KR"';}?>
                                    <?php if($rc['TI']==1){echo 'TI="TI"';}?>
                                    <?php if($rc['price']==0){echo '0="0"';}?>
                                    <?php if($rc['price']==1){echo '1="1"';}?>
                                    <?php if($rc['price']==2){echo '2="2"';}?>
                                    >
                    <div class="card carda" style="width: 100%" id="card<?php echo $colcount; ?>">
                        <img src="img/<?php echo $rc['restaurantID']?>.jpg" class="img-thumbnail" alt="..." style="height:15em;padding:0">
                        <div class="card-body" style=" padding-top: 0px;">
                            <h5 class="card-title" style="margin-top:0"><h4 style="margin:0; font-size:19px;"align="center"> <?php echo $name ?> </h4></h5>
                            <div class="row d-flex justify-content-center bd-highlight" style="margin:0 0px 0 0px">
                            <?php
                            if ($rc['JP']==1){echo '<div class="btn btn-warning d-flex justify-content-between" align="right" style="padding:4px;width:47px;height:20px; margin:4px 0px 0 4px; display:flex;flex-direction:row;align-items:center"><h6 style="font-size:7px">Japan Food</h6></div>';}
                            if ($rc['HK']==1){echo '<div class="btn btn-warning d-flex justify-content-between" align="right" style="padding:4px;width:63px;height:20px; margin:4px 0px 0 4px; display:flex;flex-direction:row;align-items:center"><h6 style="font-size:7px">Hong Kong Food</h6></div>';}
                            if ($rc['KR']==1){echo '<div class="btn btn-warning d-flex justify-content-between" align="right" style="padding:4px;width:45px;height:20px; margin:4px 0px 0 4px; display:flex;flex-direction:row;align-items:center"><h6 style="font-size:7px">Korea Food</h6></div>';}
                            if ($rc['TI']==1){echo '<div class="btn btn-warning d-flex justify-content-between" align="right" style="padding:4px;width:45px;height:20px; margin:4px 0px 0 4px; display:flex;flex-direction:row;align-items:center"><h6 style="font-size:7px">Thai Food</h6></div>';}

                            echo'</div><div class="row d-flex justify-content-center bd-highlight" style="margin:0 0px 6px 0px">';
                            if ($rc['HKI']==1){echo '<div class="btn btn-primary d-flex justify-content-between" align="right" style="padding:4px;width:67px;height:20px; margin:4px 0px 0 4px; display:flex;flex-direction:row;align-items:center"><h6 style="font-size:7px">Hong Kong Island</h6></div>';}
                            if ($rc['KL']==1){echo '<div class="btn btn-primary d-flex justify-content-between" align="right" style="padding:4px;width:50px;height:20px; margin:4px 0px 0 4px; display:flex;flex-direction:row;align-items:center"><h6 style="font-size:7px">Kowloon</h6></div>';}
                            if ($rc['OI']==1){echo '<div class="btn btn-primary d-flex justify-content-between" align="right" style="padding:4px;width:50px;height:20px; margin:4px 0px 0 4px; display:flex;flex-direction:row;align-items:center"><h6 style="font-size:7px">Outlying Islands</h6></div>';}
                            if ($rc['NT']==1){echo '<div class="btn btn-primary d-flex justify-content-between" align="right" style="padding:4px;width:55px;height:20px; margin:4px 0px 0 4px; display:flex;flex-direction:row;align-items:center"><h6 style="font-size:7px">New Territories</h6></div>';}
                            echo'</div>';
                            ?>

                            <hr style="margin:2px">
                            <div class="form-group d-flex justify-content-between"style="margin:0"><h5>Rating</h5>
                            <?php echo '<div class="row" style="align-items:center;margin:0">';  for($i = 0 ;$i<$rc['RANK'];$i++){echo '<span class="fa fa-star"></span>';} for($i = 0 ;$i<5-$rc['RANK'];$i++){echo '<span class="fa fa-star-o"></span>';} echo '</div>' ?> </div>
                            <div class="form-group d-flex justify-content-between"style="margin:0"><h5>Price</h5><?php if($rc['price']==0){echo "Less than $100";}else if($rc['price']==1){echo "$100-$200";}else{echo "More than $200";} ?></div>
                            <hr style="margin:0px 0 0px 0 ">
                            <i align="center" style="background-color:#6c757d;display:flex;justify-content:center;color:white">Location</i>
                            <hr style="margin:0px 0 8px 0 ">
                                <div class="form-group d-flex justify-content-between" style="width=100% ;margin-bottom:4px">
                                <i class="fa fa-paper-plane" style="align-items:center; margin:2px 6px 0px 4px;font-size:13px"></i><i style="margin:0 ; font-size:12px"align="center" ><?php echo $rc['Address']; ?> <div align="right"><i style="color:cornflowerblue"align="right"> Tel:<?php echo $rc['telephone']; ?></i></div></i>
                                </div>
                                <?php if($rc['Address2']!=""){?>
                            <hr style="margin:0px 0 8px 0 ">
                                <div class="form-group d-flex justify-content-between" style="width=100%;margin-bottom:10px">
                                <i class="fa fa-paper-plane" style="align-items:center; margin:2px 6px 0px 4px;font-size:13px"></i><i style="margin:0 ; font-size:12px"align="center" ><?php echo $rc['Address2']; ?> <div align="right"><i style="color:cornflowerblue"align="right"> Tel:<?php echo $rc['telephone2']; ?></i></div></i>
                                </div>
                                <?php } ?>
                                <i align="center" style="background-color:#6c757d;display:flex;justify-content:center;color:#6c757d">.</i>
                                <hr style="margin:0px 0 8px 0 ">

                                    <button class="btn btn-info" style="width:100%"type="button" id="button-addon1" onClick="myFunction('<?php echo $rc['restaurantID'] ?>')" >View Menu</button>

                        </div>
                    </div>
                </article>

                <?php
                }
                ?>
            </div>

        </div>




            </div>
            <p id="Addresstag" class="form-group form-check tagChose" location="hki">Hong Kong Island</p>
            <p id="Addresstag" class="form-group form-check tagChose" location="kl">Kowloon</p>
            <p id="Addresstag" class="form-group form-check tagChose" location="nt">New Territories</p>
            <p id="Addresstag" class="form-group form-check tagChose" location="oi">Outlying Islands</p>
            <!--  -->
            <p id="Cuisinels" class="form-group form-check tagChose" food="jp">Japan Food</p>
            <p id="Cuisinels" class="form-group form-check tagChose" food="hk">Hong Kong Food</p>
            <p id="Cuisinels" class="form-group form-check tagChose" food="kr">Korea Food</p>
            <p id="Cuisinels" class="form-group form-check tagChose" food="ti">Thai Food</p>

            <!-- Footer -->
            <div class="d-flex justify-content-center">            <ul class="pagination">
                          <li><a href="#">&laquo;</a></li>
                          <?php if(isset($_GET['page']) ){  ?>
                            <?php if($_GET['page']==1 ){ ?>
                            <li class="active"><a href="main.php?page=1">1</a></li>
                            <li><a href="main.php?page=2">2</a></li>
                            <?php  }else{ ?>
                              <li><a href="main.php?page=1">1</a></li>
                              <li class="active"><a href="main.php?page=2">2</a></li>
                          <?php  }}else{ ?>
                          <li class="active"><a href="main.php?page=1">1</a></li>
                          <li><a href="main.php?page=2">2</a></li>
                      <?php } ?>
                      <li><a href="#">&raquo;</a></li>
                      </ul></div>
<footer class="page-footer  mdb-color pt-4" style="background-color:#343a40;color:whitesmoke;margin-top:0em">

  <!-- Footer Links -->
  <div class="container text-center text-md-left">

    <!-- Footer links -->
    <div class="row text-center text-md-left mt-3 pb-3">

      <!-- Grid column -->
      <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
        <h6 class="text-uppercase mb-4 font-weight-bold">Company name</h6>
        <p>Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet,
          consectetur
          adipisicing elit.</p>
      </div>
      <!-- Grid column -->

      <hr class="w-100 clearfix d-md-none">

      <!-- Grid column -->
      <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
        <h6 class="text-uppercase mb-4 font-weight-bold">Products</h6>
        <p>
          <a href="#!">MDBootstrap</a>
        </p>
        <p>
          <a href="#!">MDWordPress</a>
        </p>
        <p>
          <a href="#!">BrandFlow</a>
        </p>
        <p>
          <a href="#!">Bootstrap Angular</a>
        </p>
      </div>
      <!-- Grid column -->

      <hr class="w-100 clearfix d-md-none">

      <!-- Grid column -->
      <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
        <h6 class="text-uppercase mb-4 font-weight-bold">Useful links</h6>
        <p>
          <a href="#!">Your Account</a>
        </p>
        <p>
          <a href="#!">Become an Affiliate</a>
        </p>
        <p>
          <a href="#!">Shipping Rates</a>
        </p>
        <p>
          <a href="#!">Help</a>
        </p>
      </div>

      <!-- Grid column -->
      <hr class="w-100 clearfix d-md-none">

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
        <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
        <p>
          <i class="fas fa-home mr-3"></i> New York, NY 10012, US</p>
        <p>
          <i class="fas fa-envelope mr-3"></i> info@gmail.com</p>
        <p>
          <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
        <p>
          <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Footer links -->

    <hr>

    <!-- Grid row -->
    <div class="row d-flex justify-content-between ">

      <!-- Grid column -->
      <div class="col-md-7 col-lg-8">

        <!--Copyright-->
        <p class="text-center text-md-left">© 2018 Copyright:
          <a href="https://mdbootstrap.com/education/bootstrap/">
            <strong> MDBootstrap.com</strong>
          </a>
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->

        <!-- Social buttons -->
        <div class="d-flex justify-content-end">
          <ul class="list-unstyled list-inline">
            <li class="list-inline-item">
              <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-google-plus-g"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>

      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

</footer>
<!-- Footer -->
        </div>



    </div>
    </div>
    </div>
    <div id="catchCard1"  style="display:none"></div>
    <div id="catchCard2"  style="display:none"></div>
    <div id="catchCard3" style="display:none"></div>
    <div id="catchCard4" style="display:none"></div>
    <div id="catchCard5" style="display:none"></div>
    <div id="catchCard6" style="display:none"></div>
    <script>
        function myFunction(id) {
            window.location.href = "menu.php?id="+id;
        }
    </script>

<script>
var x = document.getElementById("demo");

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.watchPosition(showPosition);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}
    
function showPosition(position) {
    x.value="20 Tsing Yi Road, Tsing Yi Island, N.T";
}
</script>

</body>


</html>
